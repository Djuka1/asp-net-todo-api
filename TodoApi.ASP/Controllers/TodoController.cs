﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using TodoApi.ASP.Models;

namespace TodoApi.ASP.Controllers
{
	[Route("api/[controller]")]
    public class TodoController : Controller
    {
		private readonly TodoContext _context;

		public TodoController(TodoContext context)
		{
			_context = context;
			if(_context.TodoItems.Count() == 0)
			{
				_context.TodoItems.Add(new TodoItem { Name = "Learn ASP.NET", IsComplete=true });
				_context.TodoItems.Add(new TodoItem { Name = "Create Git Repo", IsComplete=true });
				_context.TodoItems.Add(new TodoItem { Name = "Create Local Branches", IsComplete=true });
				_context.TodoItems.Add(new TodoItem { Name = "Read Microsoft Documentation", IsComplete=true });
				_context.TodoItems.Add(new TodoItem { Name = "Create Example API Project", IsComplete=true });
				_context.TodoItems.Add(new TodoItem { Name = "Finish Example API Project", IsComplete=true });
				_context.SaveChanges();
			}
		}

		[HttpGet("{id}", Name = "GetTodo")]
		public IActionResult GetById(long id)
		{
			TodoItem item = _context.TodoItems.Find(id);
			if (item == null)
			{
				return NotFound();
			}
			return Ok(item);
		}

		[HttpGet]
		public List<TodoItem> GetAll()
		{
			return _context.TodoItems.ToList();
		}
		
		[HttpPost]
		public IActionResult Post([FromBody] TodoItem todoItem)
			{
			if (todoItem == null)
			{
				return BadRequest();
			}
			_context.TodoItems.Add(todoItem);
			_context.SaveChanges();

			// Returns a 201 response. HTTP 201 is standard response for an HTTP POST method that creates a new resource on the server.
			// Adds a location header to the response. The location header specifies the URI of the newly craeted to-do item.
			// Uses the GetTodo named route to create the URL. GetTodo is the named route defined in GetById
			return CreatedAtRoute("GetTodo", new { id=todoItem.Id }, todoItem);
		}

		// Response is No content
		// By HTTP specification, PUT requests that the clinet sends the entire updated entity, not just deltas.
		// Use HTTP PATCH for partial updates
		[HttpPut("{id}")]
		public IActionResult Put(long id, [FromBody] TodoItem item)
		{
			if(item == null)
			{
				return BadRequest();
			}

			TodoItem todo = _context.TodoItems.Find(id);
			if(todo == null)
			{
				return NotFound();
			}

			todo.IsComplete = item.IsComplete;
			todo.Name = item.Name;

			_context.TodoItems.Update(todo);
			_context.SaveChanges();
			return NoContent();
		}

		// Delete response is 204 NO CONTENT
		[HttpDelete("{id}")]
		public IActionResult Delete(long id)
		{
			TodoItem item = _context.TodoItems.Find(id);
			if(item == null)
			{
				return NotFound();
			}

			_context.TodoItems.Remove(item);
			_context.SaveChanges();
			return NoContent();
		}

	}
}
